# Swerty Console Keymap #

An implementation of the Swerty keyboard layout by Johan E. Gustafsson for the GNU/Linux console.

### Installation ###

1. Install Gentoo
2. Copy **se-swerty.map.gz** to **/usr/share/keymaps/i386/qwerty/**
3. Change the keymap in **/etc/conf.d/keymaps** to `keymap="se-swerty"`
4. If you haven't already, add keymaps to the boot runlevel with `rc-update add keymaps boot`
5. Change the font in **/etc/conf.d/consolefont** to `consolefont="lat9w-16"

### Disclaimer ###

As I only vaguely understand the keymap file format, there may be errors and omissions when it comes to modifier combinations, dead keys etc. If you come across any, feel free to suggest fixes.